#!/usr/bin/env bash
mkdir -p build
sassc -a -M -t compact gtk.scss build/gtk.css
sudo mkdir -p /usr/share/themes/Fluent/gtk-4.0
sudo install -m755 build/gtk.css /usr/share/themes/Fluent/gtk-4.0